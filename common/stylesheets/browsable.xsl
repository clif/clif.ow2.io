<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  
  <xsl:include href="./common.xsl"/>
  
  <!-- ========================================================================= -->  
  <!-- Process an entire document into an HTML page -->
  <!-- ========================================================================= --> 
  <xsl:template match="document">
    
    <xsl:variable name="project" select="document($project-file)/project"/>
    <xsl:variable name="filename" select="properties/filename"/>
    <xsl:variable name="html-filename">
      <xsl:value-of select="substring-before($filename,'.xml')"/>
      <xsl:value-of select="$printable-html-suffix"/>
    </xsl:variable>
    <xsl:variable name="projectname">
	  <xsl:value-of select="$project/title"/>
	</xsl:variable>
    <xsl:variable name="path-to-root" select="properties/pathtoroot"/>       
    <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
        <meta http-equiv="Content-Language" content="en"/>
        <title><xsl:value-of select="$project/title"/> - <xsl:value-of select="properties/title"/></title>
	<xsl:variable name="description">
	  <xsl:value-of select="$project/description"/>
	</xsl:variable>
        <meta content="{$description}" name="Description"/>
	<xsl:variable name="keywords">
	  <xsl:value-of select="$project/keywords"/>
	</xsl:variable>
        <meta content="{$keywords}" name="Keywords"/>
        <meta content="webmaster@ow2.org" name="Reply-to"/>
        <meta content="OW2" name="Owner"/>
        <meta content="index, follow" name="Robots"/>	
        <xsl:for-each select="properties/author">
          <xsl:variable name="name">
            <xsl:value-of select="."/>
          </xsl:variable>
          <xsl:variable name="email">
            <xsl:value-of select="@email"/>
          </xsl:variable>
          <meta name="author" content="{$name}"/>
          <meta name="email" content="{$email}"/>
        </xsl:for-each>
        <script type="text/javascript" src="{$path-to-root}/js/objectweb.js"></script>
        <link id="stylesheet" rel="stylesheet" href="{$path-to-root}/common.css" type="text/css" />
        <link rel="icon" href="{$path-to-root}/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="{$path-to-root}/images/favicon.ico" type="image/x-icon" />
      </head>

      <body class="bodyproject" bgcolor="#FFFFFF" onload="MM_preloadImages('{$path-to-root}/images/menu/boutonok2.gif','{$path-to-root}/images/menu/boutonsearch2.gif','{$path-to-root}/images/menu/boutonprint2.gif','{$path-to-root}/images/menu/boutonsubscribe2.gif')" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >

        <table border="0" width="100%" cellspacing="0" cellpadding="0">
	  <tr>
            <td width="253" valign="top"><a href="http://www.ow2.org/"><img src="{$path-to-root}/images/logo-ow2.png" width="253" height="70" border="0" alt="OW2 Consortium"/></a></td>
            <td valign="top">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td valign="top"><img src="{$path-to-root}/images/pix.gif" width="395" height="20" alt=" "/></td>
                <td width="150" valign="top"><img src="{$path-to-root}/images/pix.gif" width="150" height="20" alt=" "/></td>
              </tr>

              <tr>
                <td valign="top">
                  <table border="0" cellspacing="0" cellpadding="5">
                    <tr>
                      <!-- menu -->
                      <td valign="top"></td>
                      <td valign="top"></td>
                      <td valign="top"></td>
                      <td valign="top"></td>
                    </tr>
                  </table>
                </td>
                
		<td width="150" valign="top" style="line-height: 20px;">
		<form method="get" action="http://www.google.com/custom" target="_blank">
		<input type="hidden" value="S:http://www.ow2.org/;GL:0;AH:center;LH:70;L:http://www.ow2.org/xwiki/skins/ow2/logo.png;LW:500;AWFID:48faba182d01f379;" name="cof"/>
		<input type="hidden" value="ow2.org;mail-archive.ow2.org" name="domains"/>
		<input type="hidden" value="ow2.org" name="sitesearch"/>
		<table colspan="0" rowspan="0"><tr><td><input type="text" name="q" size="10" maxlength="255" value="" onfocus="this.value=''" onblur="if (this.value=='') this.value='search'"/></td><td><input type="image" name="sa" id="search" src="{$path-to-root}/images/menu/boutonsearch1.gif" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('search','','{$path-to-root}/images/menu/boutonsearch2.gif',1)" alt="Submit"/></td><td><a href="{$html-filename}" target="_blank" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('print','','{$path-to-root}/images/menu/boutonprint2.gif',1)" alt="Submit"><img src="{$path-to-root}/images/menu/boutonprint1.gif" alt="Print" name="print" width="20" height="20" border="0" id="print"/></a></td></tr></table>
<p class="glose"><strong><a class="lienglose" target="_blank" href="http://www.google.com/advanced_search?q=+site:ow2.org">Advanced Search</a></strong> - Powered by Google</p>
		</form>
		</td>
		</tr>
            </table>
          </td>
        </tr>
          <tr>
            <td colspan="2" valign="bottom" bgcolor="#FF6600" width="100%">
		&#160;<a href="http://www.ow2.org/" class="barre" target="_blank">&#160;Consortium&#160;</a>
		&#160;&#160;<a href="http://www.ow2.org/xwiki/bin/view/Activities/Fundamentals" class="barre" target="_blank">&#160;Activities&#160;</a>
		&#160;&#160;<a href="http://www.ow2.org/xwiki/bin/view/Activities/Projects" class="barre" target="_blank">&#160;Projects&#160;</a>
		&#160;&#160;<a href="http://forge.objectweb.org/" class="barre" target="_blank">&#160;Forge&#160;</a>
		&#160;&#160;<a href="http://www.ow2.org/view/Events/" class="barre" target="_blank">&#160;Events&#160;</a>
	    </td>
          </tr>
      </table>
  
  <table summary="" width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="20" valign="top"><img src="{$path-to-root}/images/pix.gif" width="20" height="1" alt=" "/></td>
     
      <td width="160" valign="top">
        <img src="{$path-to-root}/images/pix.gif" width="160" height="1" alt=" "/>
        <br/>
        <p style="font-size: 16px; font-weight: bold; color: #FF6600; border-bottom: 4px solid #0099CC"><xsl:value-of select="$project/title"/></p><br />
        <xsl:apply-templates select="$project/body/menu">
          <xsl:with-param name="path-to-root"><xsl:value-of select="$path-to-root"/></xsl:with-param>
        </xsl:apply-templates>
      </td>

      <td valign="top" width="100%" align="left">
        <xsl:apply-templates select="body/s1"/>
      </td>

      <td width="10" valign="top"><img src="{$path-to-root}/images/pix.gif" width="10" height="1" alt=" "/></td>
    </tr> 

    <tr>
      <td width="20" valign="top"><img src="{$path-to-root}/images/pix.gif" width="20" height="1" alt=" "/></td>
      <td width="160" valign="top"><img src="{$path-to-root}/images/pix.gif" width="160" height="1" alt=" "/></td>
      <td valign="top"><img src="{$path-to-root}/images/pix.gif" width="465" height="1" alt=" "/><br/><address>Copyright &#169; 1999-2009, <a href="http://www.ow2.org/">OW2 Consortium</a> | <a href="http://wiki.opalval.ow2.org/xwiki/bin/view/Main/Contact">contact</a> | <a href="mailto:webmaster@ow2.org">webmaster</a> | Last modified at <xsl:value-of select="$last-modified"/></address></td>
      <td width="10" valign="top"><img src="{$path-to-root}/images/pix.gif" width="10" height="1" alt=" "/></td>
    </tr>
  </table>
  <br/>
</body>
</html>
</xsl:template>

  <!-- ========================================================================= -->
  <!-- menu for the navigation bar -->
  <!-- ========================================================================= --> 
  <xsl:template match="menu">
    
    <xsl:param name="path-to-root">.</xsl:param>
    <div class="OW2menu">
      <xsl:apply-templates select="legend | a | connect">
        <xsl:with-param name="path-to-root">
          <xsl:value-of select="$path-to-root"/>
        </xsl:with-param>      
      </xsl:apply-templates>
      <xsl:apply-templates select="menuitem">
        <xsl:with-param name="path-to-root">
	  <xsl:value-of select="$path-to-root"/>
	</xsl:with-param>      
      </xsl:apply-templates>
    </div><br />
  </xsl:template>

  <!-- ========================================================================= --> 
  <!-- menuitem for the navigation bar -->
  <!-- ========================================================================= --> 
  <xsl:template match="menuitem">
    <xsl:param name="path-to-root">.</xsl:param>
    <xsl:apply-templates>	
      <xsl:with-param name="path-to-root">
	<xsl:value-of select="$path-to-root"/>
      </xsl:with-param>      
    </xsl:apply-templates>
  </xsl:template>

  <!-- ========================================================================= --> 
  <!-- menu/legend -->
  <!-- ========================================================================= --> 
  <xsl:template match="menu/legend">
    <div class="OW2menutitre"><xsl:apply-templates/></div>
  </xsl:template>

  <!-- ========================================================================= --> 
  <!-- menu/connect -->
  <!-- ========================================================================= --> 
  <xsl:template match="menu/connect">
    
    <xsl:param name="path-to-root">.</xsl:param>
    <xsl:variable name="path">
      <xsl:value-of select="$path-to-root"/>
    </xsl:variable>
    
    <xsl:variable name="htmlref">
      <xsl:value-of select="substring-before(@href,'.xml')"/>
      <xsl:value-of select="$html-suffix"/>
      <xsl:value-of select="substring-after(@href,'.xml')"/>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="starts-with(@href,'/') or starts-with(@href,'http://') or starts-with(@href,'mailto:') or starts-with(@href,'ftp://')">
        <a href="{$htmlref}" class="menutitre"><xsl:apply-templates/></a>
      </xsl:when>
      
      <xsl:otherwise>
        <a href="{$path}/{$htmlref}" class="menutitre"><xsl:apply-templates/></a>
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>

  <!-- ========================================================================= --> 
  <!-- menuitem/connect -->
  <!-- ========================================================================= --> 
  <xsl:template match="menuitem/connect">
    
    <xsl:param name="path-to-root">.</xsl:param>
    <xsl:variable name="path">
      <xsl:value-of select="$path-to-root"/>
    </xsl:variable>
    
    <xsl:variable name="htmlref">
      <xsl:value-of select="substring-before(@href,'.xml')"/>
      <xsl:value-of select="$html-suffix"/>
      <xsl:value-of select="substring-after(@href,'.xml')"/>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="starts-with(@href,'/') or starts-with(@href,'http://') or starts-with(@href,'mailto:') or starts-with(@href,'ftp://')">
        &#183;&#160;<a href="{$htmlref}" class="menu"><xsl:apply-templates/></a><br />
      </xsl:when>
      
      <xsl:otherwise>
        &#183;&#160;<a href="{$path}/{$htmlref}" class="menu"><xsl:apply-templates/></a><br />
      </xsl:otherwise>
    </xsl:choose>
    
  </xsl:template>

  <!-- ========================================================================= --> 
  <!-- menu/a -->
  <!-- ========================================================================= --> 
  <xsl:template match="menu/a">
    
    <xsl:param name="path-to-root">.</xsl:param>
    <xsl:variable name="path">
      <xsl:value-of select="$path-to-root"/>
    </xsl:variable>
    
    <xsl:variable name="target">
      <xsl:choose>
	<xsl:when test="@fork='true'">_blank</xsl:when>
	<xsl:otherwise>_self</xsl:otherwise>
      </xsl:choose>
   </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="starts-with(@href,'/') or starts-with(@href,'http://') or starts-with(@href,'mailto:') or starts-with(@href,'ftp://')">
        <a href="{@href}" target="{$target}" class="menutitre"><xsl:apply-templates/></a>
      </xsl:when>
      
      <xsl:otherwise>
        <a href="{$path}/{@href}" target="{$target}" class="menutitre"><xsl:apply-templates/></a>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!-- ========================================================================= --> 
  <!-- menuitem/a -->
  <!-- ========================================================================= --> 
  <xsl:template match="menuitem/a">
    
    <xsl:param name="path-to-root">.</xsl:param>
    <xsl:variable name="path">
      <xsl:value-of select="$path-to-root"/>
    </xsl:variable>
    
    <xsl:variable name="target">
      <xsl:choose>
	<xsl:when test="@fork='true'">_blank</xsl:when>
	<xsl:otherwise>_self</xsl:otherwise>
      </xsl:choose>
   </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="starts-with(@href,'/') or starts-with(@href,'http://') or starts-with(@href,'mailto:') or starts-with(@href,'ftp://')">
        &#183;&#160;<a href="{@href}" target="{$target}" class="menu"><xsl:apply-templates/></a><br />
      </xsl:when>
      
      <xsl:otherwise>
        &#183;&#160;<a href="{$path}/{@href}" target="{$target}" class="menu"><xsl:apply-templates/></a><br />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

 <!-- ========================================================================= --> 
 <!-- connect -->
 <!-- ========================================================================= --> 
 <xsl:template match="connect">

   <xsl:variable name="htmlref">
     <xsl:value-of select="substring-before(@href,'.xml')"/>
     <xsl:value-of select="$html-suffix"/>
     <xsl:value-of select="substring-after(@href,'.xml')"/>
   </xsl:variable>
 
   <a href="{$htmlref}"><xsl:apply-templates/></a>
 </xsl:template>

</xsl:stylesheet>
