<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>

<!DOCTYPE document SYSTEM '../../../common/dtd/objectweb.dtd'>

<document>

  <properties>
    <author email="clif@ow2.org">CLIF team</author>
    <title>Documentation</title>
    <filename>index.xml</filename>
    <pathtoroot>..</pathtoroot>
  </properties>

<body>
	<s1 name="Main documentation">
		<p><strong>The <a href="CLIF_flyer.pdf">CLIF project's flyer</a></strong></p>
		<s2 name="Videos">
			<ul>
				<li><a href="http://www.ow2.org/view/Webinars/Testing_your_application_performance_with_CLIF">OW2 webinar</a> presenting the main features of CLIF.
				The CLIF test project archive used in this webinar is available for <a href="ow2webinar2014.zip">download</a>.</li>
				<li><a href="clif-install.htm">Installation of the main CLIF console</a>.</li>
			</ul>
		</s2>
		<s2 name="User guides">
			<ul>
				<li><a href="user_manual/manual/InstallationManual.pdf">Installation guide</a></li>
				<li><a href="user_manual/manual/QuickStart.pdf">Quick Start manual</a></li>
				<li><a href="user_manual/manual/UserManual.pdf">User manual</a></li>
				<li><a href="/clif-legacy/idoc/">ISAC plug-ins reference manual</a></li>
				<li><a href="user_manual/tutorials/UseClifTutorial.pdf">Tutorial for CLIF and ISAC utilization</a></li>
				<li><a href="http://wiki.jenkins-ci.org/display/JENKINS/CLIF+Performance+Testing+Plugin">Using CLIF with Jenkins automation server</a></li>
			</ul>
		</s2>
		<s2 name="Examples and Quick Tour tutorial">
			<ul>
				<li>A Quick Tour with the tutorial given at 2010 OW2 Annual Conference:
					<ul>
						<li>Slides [<a href="tutorial/OW2Con10-tutorial-CLIF.odp">ODP</a>|<a href="OW2Con10-tutorial-CLIF.pdf">PDF</a>]</li>
						<li>Dummy project: <a href="tutorial/Dummy.zip">Dummy.zip</a></li>
						<li>solutions to WebLab project: <a href="tutorial/WebLab-solutions.zip">WebLab-solutions.zip</a></li>
					</ul>
				</li>
				<li>Sample CLIF test plans and associated ISAC scenarios are available in this zip file: <a href="/clif-legacy/download/clif-examples.zip">clif-examples.zip</a>
					<p>You may just unzip this file to get the test plan and scenario files, or, when using the Eclipse-based console,
					you may import the zip file as an Eclipse project archive file
					(File&gt;Import...&gt;General&gt;Existing Projects into Workspace&gt;Select archive file).
					<br/>In the latter case, you directly get a CLIF test project including all the test plans and scenarios.
					</p>
				</li>
			</ul>
		</s2>
		<s2 name="Advanced users and developers documentation">
			<ul>
				<li><a href="user_manual/manual/DeveloperManual.pdf">Developer's guide</a></li>
				<li><a href="user_manual/tutorials/ExtensionIsacTutorial.pdf">Tutorial for developing new ISAC plug-ins</a></li>
				<li>architecture description [<a href="architectureV2.odp" >ODP</a>] [<a href="architectureV2.pdf" >PDF</a>]</li>
				<li><a href="/clif-legacy/jdoc/">Java API</a> documentation</li>
			</ul>
			<subtitle>A Fractal component-based architecture</subtitle>
			<p>To provide a flexible architecture, CLIF makes use of the <a href="https://fractal.ow2.io/">Fractal</a> component model. For example, load injectors and probes are components that can be defined, plugged in and (un-/re-)deployed at runtime. Thanks to the Fractal model, customizing CLIF does not require to know details about the whole framework, but only about a well-defined, reduced number of client and server interfaces.</p>
		</s2>
	</s1>
	<s1 name="Related papers, presentations and reports">
	<ul>
		<li><a href="https://www.ow2con.org/view/2018/"><img alt="OW2con′18 Annual conference: A Journey Between Values and Value" src="https://www.ow2con.org/download/2018/Help_To_Promote/Button-OW2con18-Short75x33.png" width="75" height="33" border="0"/></a>
			<a href="CLIF AllInOne.pdf">All-in-one functional, integration, performance testing and QoE monitoring with CLIF</a>
			<br/>See the presentation <a href="https://www.ow2con.org/view/2018/Videos#HAll-in-onetoolfunctional2Cintegration2CperformancetestingandQoEmonitoringwithCLIF2CBrunoDillenseger2COrange">presentation video</a>
		</li>
		<li><a href="https://www.ow2con.org/view/2017/"><img alt="OW2con′17 Annual conference: New Challenges of Mainstream Open Source Software" src="https://www.ow2con.org/download/2017/Help_To_Promote/Button-OW2con17-Short-57x24.png" width="57" height="24" border="0"/></a>
			<a href="CLIF guided tour.pdf">Performance Testing and Monitoring: A Guided Tour Through CLIF Tools and Features</a>
			<br/>See the presentation <a href="https://www.ow2con.org/view/2017/Videos#HPerformancetestingandmonitoring:aguidedtourthroughCLIFtoolsandfeatures2CBrunoDillenseger2COrangeLabs">presentation video</a>
		</li>
		<li><a href="http://www.ow2con.org/bin/view/2016/"><img alt="OW2con'16 Annual conference: Code to Product" src="http://www.ow2con.org/bin/download/2016/Help_To_Promote/Button-OW2con16-Short75x33.png" width="75" height="33" border="0"/></a>
			<a href="ProActive CLIF with workflows.pdf">Automating Performance Testing over the Cloud with CLIF and ProActive Workflows &amp; Scheduling</a>
			<br/>See the presentation <a href="https://www.ow2con.org/bin/view/2016/Videos#HAutomatingperformancetestingoverthecloudwithCLIFandProActiveWorkflows2CBrianAmedro2CDenisCaromel2CBrunoDillenseger2CActiveEon2COrange">video and the slides</a>
		</li>
		<li><a href="http://www.ow2con.org/bin/view/2015/"><img alt="OW2con'15 Annual conference: The Drive for Open Source Governance" src="http://www.ow2con.org/bin/download/2015/Help_To_Promote/Button-OW2con15-Short75x33.png" border="0" width="75" height="33"/></a>
			<a href="benchmark Orange Forge.pdf">Benchmarking Orange Forge with CLIF</a>
			<br/>See the presentation <a href="https://www.ow2con.org/bin/view/2015/Videos#HBenchmarkingOrangeForgewithCLIF2CBrunoDillenseger2COrange">video and the slides</a>
		</li>
		<li><a href="http://www.ow2.org/bin/view/OW2con-2014/Presentations"><img src="http://skins.ow2.org/images/ow2con14/buttonOW2con14Short75x33.png" border="0" width="75" height="33" alt="OW2con'14 Annual conference: Leveraging the Global Open Source Ecosytem"/></a>
			<a href="re-VAMP CLIF.pdf">Re-VAMP load testing with CLIF for continuous integration on the cloud</a>
			<br/>See the screen capture video of the <a href="T3_2_4.htm">demonstration</a>, the <a href="http://www.slideshare.net/slideshow/embed_code/41548517">slides <img src="../images/SlideShare.png" align="bottom" border="0" width="16" height="16" alt="SlideShare"/></a> and the <a href="https://www.youtube.com/embed/HmAEhcODy74?autoplay=1">presentation video <img src="http://www.youtube.com/favicon.ico" align="bottom" border="0" width="16" height="16" alt="YouTube"/></a>.
		</li>
		<li><a href="http://www.ow2.org/view/OW2Con-2013/"><img src="http://skins.ow2.org/skins/skinOW2/images/buttonOW2con13SquareSmall.png" border="0" width="75" height="32" alt="OW2con'13 Annual conference: Open Source Collaborative Innovation, The Way Forward"></img></a>
			<a href="http://skins.ow2.org/images/ow2con13/Slides/Bruno_Dillenseger.pdf">CLIF meets Jenkins: performance testing in continuous integration, or more...</a>
			<br/>Slides:
				<a href="http://www.slideshare.net/slideshow/embed_code/28481586"><img src="../images/SlideShare.png" align="bottom" border="0" width="16" height="16" alt="SlideShare"/></a>,
			Videos:
				<a href="http://www.dailymotion.com/embed/video/x17gklz_ow2con-13-presentation-bruno-dillenseger_tech"><img src="../images/DailyMotion.png" align="bottom" border="0" width="16" height="16" alt="DailyMotion"/></a>
				<a href="http://www.youtube.com/embed/KozionVRN_k"><img src="http://www.youtube.com/favicon.ico" align="bottom" border="0" width="16" height="16" alt="YouTube"/></a>
		</li>
		<li><a href="ComPAS2013.pdf">Talk on using CLIF on a PaaS cloud via the Jenkins continuous integration server</a> given at the <a href="http://compas2013.inrialpes.fr/">ComPAS 2013 Conference</a></li>
		<li><a href="phD_ElHachemiBendahmane.pdf">PhD on self-optimization (in French)</a> with CLIF+SelfBench</li>
		<li><a href="http://www.ow2.org/view/OW2Con-2012/"><img src="http://skins.ow2.org/skins/skinOW2/images/buttonOW2con12Square.png" border="0" width="75" height="33" alt="buttonOW2con12Square.png"/></a>
			<a href="http://skins.ow2.org/images/OW2con12/pdf/BrunoDillensegerProActiveCLIF-ComCloud-final.pdf">ProActive CLIF: Cloud Enabled Load Injection</a>
			<br/>Slides:
				<a href="http://www.slideshare.net/slideshow/embed_code/15635511"><img src="../images/SlideShare.png" align="bottom" border="0" width="16" height="16" alt="SlideShare"/></a>,
			Videos:
				<a href="http://www.dailymotion.com/embed/video/xvnd3n"><img src="../images/DailyMotion.png" align="bottom" border="0" width="16" height="16" alt="DailyMotion"/></a>
				<a href="http://www.youtube.com/embed/JvdC-6UkxmI"><img src="http://www.youtube.com/favicon.ico" align="bottom" border="0" width="16" height="16" alt="YouTube"/></a>

			<table border="0">
				<tr>
					<td>
						<img src="../images/OW2con12-AwardGeneric.png" alt="OW2con 2013 award" border="0" height="117" width="150"/>
					</td>
					<td>
						This work won the <a href="http://www.ow2.org/view/OW2Con-2012/AwardsResults">Special Prize from OW2's Technical Committee</a>.
					</td>
				</tr>
			</table>
		</li>
		<li>technical talk about an <a href="CLIF_SOAusecase.pdf">SOA use case of CLIF</a> at <a href="http://www.ow2.org/view/OW2Con-2011/">OW2's annual conference</a>, 24th November 2011 in Paris</li>
		<li>using CLIF with an external load injection controller: A. Harbaoui, N. Salmi, B. Dillenseger, and J.-M. Vincent, <a href="http://www.computer.org/portal/web/csdl/doi/10.1109/ICAS.2010.9">"Introducing Queuing Network-Based Performance Awareness in Autonomic Systems"</a>, <em>Best paper Award</em>, The Sixth International Conference on Autonomic and Autonomous Systems (ICAS), March 7-13, 2010 - Cancun, Mexico</li>
		<li>about CLIF architecture: B. Dillenseger, "CLIF, a framework based on Fractal for flexible, distributed load testing", Annals of Telecommunications, Vol. 64, No 1-2, January-February 2009 (available from <a href="http://www.springerlink.com/content/rp8681tm25324672">SpringerLink</a>, or ask the CLIF mailing list)</li>
		<li>presentation at ObjectWebCon 2006 [<a href="ObjectWebCon06.odp">ODP</a>][<a href="ObjectWebCon06.pdf">PDF</a>]</li>
		<li>CLIF's kick-off paper at OOPSLA 2003 Workshop on "Middleware Benchmarking: Approaches, Results, Experiences" [<a href="OOPSLA2003.pdf">PDF</a>], and presentation [<a href="OOPSLA2003-slides.pdf">PDF</a>]</li>
	</ul>
	</s1>
</body>
</document>
